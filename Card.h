#ifndef Card_h
#define Card_h

#include "GameLogicHeader.h"

enum class Colour { Brown, Grey, Blue, Green, Yellow, Red, Purple, Null };
enum class Item { Clay, Wood, Stone, Class, Papyrus };
enum class SciSymbol { ArmillarySphere, Balance, Sundial, MortarAndPestle, PlumbBob, Quill, Wheel, Null };
enum class ChainSymbol { Amphora, Barrel, Mask, Temple, Sun, WaterDrop, Pillar, Moon, Target, Helmet, Horseshoe, Sword, Tower, Harp, Gear, Book, Lamp, Null };

class Card
{
protected:
	const unsigned int id;
	const std::string name;
	const Colour colour;
	const std::array<unsigned int, 5> items_prod;
	const std::array<unsigned int, 5> items_cost;
	const unsigned int coins_prod;
	const unsigned int coins_cost;
	const unsigned int vic_point;
	const unsigned int mil_power;
	const unsigned int sciSymbol;
	const unsigned int chainSymbol_prod;
	const unsigned int chainSymbol_cost;
	bool constructedFlag = false;
	bool constructibleFlag = true;
	bool discardFlag = false;

public:
	Card(int i, std::string n, Colour c, std::array<unsigned int, 5> ip, std::array<unsigned int, 5> ic, int cp, int cc, int vp, int mp, int ss, int csp, int csc) : id(i), name(n), colour(c), items_prod(ip), items_cost(ic), coins_prod(cp), coins_cost(cc), vic_point(vp), mil_power(mp), sciSymbol(ss), chainSymbol_prod(csp), chainSymbol_cost(csc), constructedFlag(false), discardFlag(false) {}
	const unsigned int& getID() { return id; }
	const std::string& getName() { return name; }
	const Colour& getColour() { return colour; }
	const std::array<unsigned int, 5>& getItemsProd() { return items_prod; }
	const std::array<unsigned int, 5>& getItemsCost() { return items_cost; }
	const unsigned int& getCoinsProd() { return coins_prod; }
	const unsigned int& getCoinsCost() { return coins_cost; }
	const unsigned int& getVicPoint() { return vic_point; }
	const unsigned int& getMilPower() { return mil_power; }
	const unsigned int& getSciSymbol() { return sciSymbol; }
	const unsigned int& getChainSymbolProd() { return chainSymbol_prod; }
	const unsigned int& getChainSymbolCost() { return chainSymbol_cost; }
	const bool& getConstructedFlag() { return constructedFlag; }
	const bool& getConstructibleFlag() { return constructibleFlag; }
	const bool& getDiscardFlag() { return discardFlag; }

	void setConstructedFlag(bool flag) { constructedFlag = flag; }
	void setConstructibleFlag(bool flag) { constructibleFlag = flag; }
	void setDiscardFlag(bool flag) { discardFlag = flag; }
};

#endif // !Card_h
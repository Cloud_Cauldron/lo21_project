﻿#ifndef Deck_h
#define Deck_h

#include "Card.h"

class Deck
{
private:
	std::vector<Card*> wonders;
	std::vector<Card*> cards_age1;
	std::vector<Card*> cards_age2;
	std::vector<Card*> cards_age3;
	std::vector<Card*> guilds;

	std::vector<Card*> deck_age1;
	std::vector<Card*> deck_age2;
	std::vector<Card*> deck_age3;

public:
	Deck()
	{
		init_Wonders();
		init_Cards_Age1();
		init_Cards_Age2();
		init_Cards_Age3();
		init_Guilds();
		init_Deck_Age1();
		init_Deck_Age2();
		init_Deck_Age3();
	}
	~Deck()
	{
		for (size_t i = 0; i < wonders.size(); i += 1) { delete wonders[i]; }
		for (size_t i = 0; i < cards_age1.size(); i += 1) { delete cards_age1[i]; }
		for (size_t i = 0; i < cards_age2.size(); i += 1) { delete cards_age2[i]; }
		for (size_t i = 0; i < cards_age3.size(); i += 1) { delete cards_age3[i]; }
		for (size_t i = 0; i < guilds.size(); i += 1) { delete guilds[i]; }
	}

	void init_Wonders()
	{
		// id, name, colour, items_prod, items_cost, coins_prod, coins_cost, vic_point, mil_power, sciSymbol, chainSymbol_prod, chainSymbol_cost
		// item id : Clay(0), Wood(1), Stone(2), Class(3), Papyrus(4)
		// SciSymbol id : Armillary Sphere(0), Balance(1), Sundial(2), Mortar And Pestle(3), Plumb Bob(4), Quill(5), Wheel(6), Null(7)
		// ChainSymbol id : Amphora(0), Barrel(1), Mask(2), Temple(3), Sun(4), WaterDrop(5), Pillar(6), Moon(7), Target(8), Helmet(9), Horseshoe(10), Sword(11), Tower(12), Harp(13), Gear(14), Book(15), Lamp(16), Null(17)
		wonders.push_back(new Card(1, "Le Circus Maximus", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 1, 2, 1, 0 }, 0, 0, 3, 1, 7, 17, 17));
		wonders.push_back(new Card(2, "Le Colosse", Colour::Null, { 0, 0, 0, 0, 0 }, { 3, 0, 0, 1, 0 }, 0, 0, 3, 2, 7, 17, 17));
		wonders.push_back(new Card(3, "Le Grand Phare", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 0, 2 }, 0, 0, 4, 0, 7, 17, 17));
		wonders.push_back(new Card(4, "Les Jardins Suspendus", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 2, 0, 1, 1 }, 6, 0, 3, 0, 7, 17, 17));
		wonders.push_back(new Card(5, "La Grande Bibliothèque", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 3, 0, 1, 1 }, 0, 0, 4, 0, 7, 17, 17));
		wonders.push_back(new Card(6, "Le Mausolée", Colour::Null, { 0, 0, 0, 0, 0 }, { 2, 0, 0, 2, 1 }, 0, 0, 2, 0, 7, 17, 17));
		wonders.push_back(new Card(7, "Le Pirée", Colour::Null, { 0, 0, 0, 0, 0 }, { 1, 2, 1, 0, 0 }, 0, 0, 2, 0, 7, 17, 17));
		wonders.push_back(new Card(8, "Les Pyramides", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 0, 3, 0, 1 }, 0, 0, 9, 0, 7, 17, 17));
		wonders.push_back(new Card(9, "Le Sphinx", Colour::Null, { 0, 0, 0, 0, 0 }, { 1, 0, 1, 2, 0 }, 0, 0, 6, 0, 7, 17, 17));
		wonders.push_back(new Card(10, "La Statue de Zeus", Colour::Null, { 0, 0, 0, 0, 0 }, { 1, 1, 1, 0, 2 }, 0, 0, 3, 1, 7, 17, 17));
		wonders.push_back(new Card(11, "Le Temple d'Artémis", Colour::Null, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1 }, 12, 0, 0, 0, 7, 17, 17));
		wonders.push_back(new Card(12, "La Via Appia", Colour::Null, { 0, 0, 0, 0, 0 }, { 2, 0, 2, 0, 1 }, 3, 0, 3, 0, 7, 17, 17));
	}
	void init_Cards_Age1()
	{
		// id, name, colour, items_prod, items_cost, coins_prod, coins_cost, vic_point, mil_power, sciSymbol, chainSymbol_prod, chainSymbol_cost
		// item id : Clay(0), Wood(1), Stone(2), Class(3), Papyrus(4)
		// SciSymbol id : Armillary Sphere(0), Balance(1), Sundial(2), Mortar And Pestle(3), Plumb Bob(4), Quill(5), Wheel(6), Null(7)
		// ChainSymbol id : Amphora(0), Barrel(1), Mask(2), Temple(3), Sun(4), WaterDrop(5), Pillar(6), Moon(7), Target(8), Helmet(9), Horseshoe(10), Sword(11), Tower(12), Harp(13), Gear(14), Book(15), Lamp(16), Null(17)
		cards_age1.push_back(new Card(13, "CHANTIER", Colour::Brown, { 0, 1, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(14, "EXPLOITATION", Colour::Brown, { 0, 1, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 1, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(15, "BASSIN ARGILEUX", Colour::Brown, { 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(16, "CAVITÉ", Colour::Brown, { 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 1, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(17, "GISEMENT", Colour::Brown, { 0, 0, 1, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(18, "MINE", Colour::Brown, { 0, 0, 1, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 1, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(19, "VERRERIE", Colour::Grey, { 0, 0, 0, 1, 0 }, { 0, 0, 0, 0, 0 }, 0, 1, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(20, "PRESSE", Colour::Grey, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 0, 0 }, 0, 1, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(21, "TOUR DE GARDE", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 1, 7, 17, 17));
		cards_age1.push_back(new Card(22, "ATELIER", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 1 }, 0, 0, 1, 0, 4, 17, 17));
		cards_age1.push_back(new Card(23, "APOTHICAIRE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 1, 0 }, 0, 0, 1, 0, 6, 17, 17));
		cards_age1.push_back(new Card(24, "DÉPÔT DE PIERRE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 3, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(25, "DÉPÔT D'ARGILE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 3, 0, 0, 7, 17, 17));
		cards_age1.push_back(new Card(26, "DÉPÔT DE BOIS", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 3, 0, 0, 7, 17, 17));

		cards_age1.push_back(new Card(27, "ÉCURIES", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 1, 0, 0, 0 }, 0, 0, 0, 1, 7, 10, 17));
		cards_age1.push_back(new Card(28, "CASERNE", Colour::Red, { 0, 0, 0, 0, 0 }, { 1, 0, 0, 0, 0 }, 0, 0, 0, 1, 7, 11, 17));
		cards_age1.push_back(new Card(29, "PALISSADE", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 1, 7, 12, 17));
		cards_age1.push_back(new Card(30, "SCRIPTORIUM", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 0, 5, 15, 17));
		cards_age1.push_back(new Card(31, "OFFICINE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 0, 3, 14, 17));
		cards_age1.push_back(new Card(32, "THÉÂTRE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 3, 0, 7, 2, 17));
		cards_age1.push_back(new Card(33, "AUTEL", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 3, 0, 7, 7, 17));
		cards_age1.push_back(new Card(34, "BAINS", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 0, 1, 0, 0 }, 0, 0, 3, 0, 7, 5, 17));
		cards_age1.push_back(new Card(35, "TAVERNE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 4, 0, 0, 0, 7, 0, 17));
	}
	void init_Cards_Age2()
	{
		// id, name, colour, items_prod, items_cost, coins_prod, coins_cost, vic_point, mil_power, sciSymbol, chainSymbol_prod, chainSymbol_cost
		// item id : Clay(0), Wood(1), Stone(2), Class(3), Papyrus(4)
		// SciSymbol id : Armillary Sphere(0), Balance(1), Sundial(2), Mortar And Pestle(3), Plumb Bob(4), Quill(5), Wheel(6), Null(7)
		// ChainSymbol id : Amphora(0), Barrel(1), Mask(2), Temple(3), Sun(4), WaterDrop(5), Pillar(6), Moon(7), Target(8), Helmet(9), Horseshoe(10), Sword(11), Tower(12), Harp(13), Gear(14), Book(15), Lamp(16), Null(17)
		cards_age2.push_back(new Card(36, "SCIERIE", Colour::Brown, { 0, 2, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(37, "BRIQUETERIE", Colour::Brown, { 2, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(38, "CARRIÈRE", Colour::Brown, { 0, 0, 2, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 2, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(39, "SOUFFLERIE", Colour::Grey, { 0, 0, 0, 1, 0 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(40, "SÉCHOIR", Colour::Grey, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(41, "MURAILLE", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 0, 2, 0, 0 }, 0, 0, 0, 2, 7, 17, 17));
		cards_age2.push_back(new Card(42, "FORUM", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 1, 0, 0, 0, 0 }, 0, 3, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(43, "CARAVANSÉRAIL", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 1, 1 }, 0, 2, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(44, "DOUANES", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 4, 0, 0, 7, 17, 17));
		cards_age2.push_back(new Card(45, "TRIBUNAL", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 2, 0, 1, 0 }, 0, 0, 5, 0, 7, 17, 17));

		cards_age2.push_back(new Card(46, "HARAS", Colour::Red, { 0, 0, 0, 0, 0 }, { 1, 1, 0, 0, 0 }, 0, 0, 0, 1, 7, 17, 10));
		cards_age2.push_back(new Card(47, "BARAQUEMENTS", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 3, 0, 1, 7, 17, 11));
		cards_age2.push_back(new Card(48, "CHAMP DE TIR", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 0, 1 }, 0, 0, 0, 2, 7, 12, 17));
		cards_age2.push_back(new Card(49, "PLACE D'ARMES", Colour::Red, { 0, 0, 0, 0, 0 }, { 2, 0, 0, 1, 0 }, 0, 0, 0, 2, 7, 8, 17));
		cards_age2.push_back(new Card(50, "BIBLIOTHÈQUE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 0 }, 0, 0, 2, 0, 5, 9, 17));
		cards_age2.push_back(new Card(51, "DISPENSAIRE", Colour::Green, { 0, 0, 0, 0, 0 }, { 2, 0, 1, 0, 0 }, 0, 0, 2, 0, 3, 17, 15));
		cards_age2.push_back(new Card(52, "ÉCOLE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 1, 0, 0, 2 }, 0, 0, 1, 0, 6, 13, 14));
		cards_age2.push_back(new Card(53, "LABORATOIRE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 1, 0, 2, 0 }, 0, 0, 1, 0, 4, 16, 17));
		cards_age2.push_back(new Card(54, "STATUE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 2, 0, 0, 0, 0 }, 0, 0, 4, 0, 7, 6, 2));
		cards_age2.push_back(new Card(55, "TEMPLE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 1, 0, 0, 1 }, 0, 0, 4, 0, 7, 4, 7));
		cards_age2.push_back(new Card(56, "AQUEDUC", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 0, 3, 0, 0 }, 0, 0, 5, 0, 7, 17, 5));
		cards_age2.push_back(new Card(57, "ROSTRES", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 0, 0 }, 0, 0, 4, 0, 7, 3, 17));
		cards_age2.push_back(new Card(58, "BRASSERIE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 6, 0, 0, 0, 7, 1, 17));
	}
	void init_Cards_Age3()
	{
		// id, name, colour, items_prod, items_cost, coins_prod, coins_cost, vic_point, mil_power, sciSymbol, chainSymbol_prod, chainSymbol_cost
		// item id : Clay(0), Wood(1), Stone(2), Class(3), Papyrus(4)
		// SciSymbol id : Armillary Sphere(0), Balance(1), Sundial(2), Mortar And Pestle(3), Plumb Bob(4), Quill(5), Wheel(6), Null(7)
		// ChainSymbol id : Amphora(0), Barrel(1), Mask(2), Temple(3), Sun(4), WaterDrop(5), Pillar(6), Moon(7), Target(8), Helmet(9), Horseshoe(10), Sword(11), Tower(12), Harp(13), Gear(14), Book(15), Lamp(16), Null(17)
		cards_age3.push_back(new Card(59, "ARSENAL", Colour::Red, { 0, 0, 0, 0, 0 }, { 3, 2, 0, 0, 0 }, 0, 0, 0, 3, 7, 17, 17));
		cards_age3.push_back(new Card(60, "PRÉTOIRE", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0 }, 0, 8, 0, 3, 7, 17, 17));
		cards_age3.push_back(new Card(61, "ACADÉMIE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 1, 1, 2, 0 }, 0, 0, 3, 0, 2, 17, 17));
		cards_age3.push_back(new Card(62, "ÉTUDE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 2, 0, 1, 1 }, 0, 0, 3, 0, 2, 17, 17));
		cards_age3.push_back(new Card(63, "CHAMBRE DE COMMERCE", Colour::Yellow, { 0, 0, 0, 0, 2 }, { 0, 0, 3, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		cards_age3.push_back(new Card(64, "PORT", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 1, 0, 1, 1 }, 0, 0, 3, 0, 7, 17, 17));
		cards_age3.push_back(new Card(65, "ARMURERIE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 0, 0, 2, 1, 0 }, 0, 0, 3, 0, 7, 17, 17));
		cards_age3.push_back(new Card(66, "PALACE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 1, 1, 1, 2, 0 }, 0, 0, 7, 0, 7, 17, 17));
		cards_age3.push_back(new Card(67, "HÔTEL DE VILLE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 2, 3, 0, 0 }, 0, 0, 7, 0, 7, 17, 17));
		cards_age3.push_back(new Card(68, "OBÉLISQUE", Colour::Blue, { 0, 0, 0, 0, 0 }, { 0, 0, 2, 1, 0 }, 0, 0, 5, 0, 7, 17, 17));

		cards_age3.push_back(new Card(69, "FORTIFICATIONS", Colour::Red, { 0, 0, 0, 0, 0 }, { 1, 0, 2, 0, 1 }, 0, 0, 0, 2, 7, 17, 17));
		cards_age3.push_back(new Card(70, "ATELIER DE SIÈGE", Colour::Red, { 0, 0, 0, 0, 0 }, { 0, 3, 0, 1, 0 }, 0, 0, 0, 2, 7, 17, 17));
		cards_age3.push_back(new Card(71, "CIRQUE", Colour::Red, { 0, 0, 0, 0, 0 }, { 2, 0, 2, 0, 0 }, 0, 0, 0, 2, 7, 17, 17));
		cards_age3.push_back(new Card(72, "UNIVERSITÉ", Colour::Green, { 0, 0, 0, 0, 0 }, { 1, 0, 0, 1, 1 }, 0, 0, 2, 0, 0, 17, 17));
		cards_age3.push_back(new Card(73, "OBSERVATOIRE", Colour::Green, { 0, 0, 0, 0, 0 }, { 0, 0, 1, 0, 2 }, 0, 0, 2, 0, 0, 17, 17));
		cards_age3.push_back(new Card(74, "JARDINS", Colour::Blue, { 0, 0, 0, 0, 0 }, { 2, 2, 0, 0, 0 }, 0, 0, 6, 0, 7, 17, 17));
		cards_age3.push_back(new Card(75, "PANTHÉON", Colour::Blue, { 0, 0, 0, 0, 0 }, { 1, 1, 0, 0, 2 }, 0, 0, 6, 0, 7, 17, 17));
		cards_age3.push_back(new Card(76, "SÉNAT", Colour::Blue, { 0, 0, 0, 0, 0 }, { 2, 0, 1, 0, 1 }, 0, 0, 5, 0, 7, 17, 17));
		cards_age3.push_back(new Card(77, "PHARE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 2, 0, 0, 1, 0 }, 0, 0, 3, 0, 7, 17, 17));
		cards_age3.push_back(new Card(78, "ARÈNE", Colour::Yellow, { 0, 0, 0, 0, 0 }, { 1, 1, 1, 0, 0 }, 0, 0, 3, 0, 7, 17, 17));
	}
	void init_Guilds()
	{
		// id, name, colour, items_prod, items_cost, coins_prod, coins_cost, vic_point, mil_power, sciSymbol, chainSymbol_prod, chainSymbol_cost
		// item id : Clay(0), Wood(1), Stone(2), Class(3), Papyrus(4)
		// SciSymbol id : Armillary Sphere(0), Balance(1), Sundial(2), Mortar And Pestle(3), Plumb Bob(4), Quill(5), Wheel(6), Null(7)
		// ChainSymbol id : Amphora(0), Barrel(1), Mask(2), Temple(3), Sun(4), WaterDrop(5), Pillar(6), Moon(7), Target(8), Helmet(9), Horseshoe(10), Sword(11), Tower(12), Harp(13), Gear(14), Book(15), Lamp(16), Null(17)
		guilds.push_back(new Card(79, "GUILDE DES COMMERÇANTS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 1, 1, 0, 1, 1 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(80, "GUILDE DES ARMATEURS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 1, 0, 1, 1, 1 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(81, "GUILDE DES BÂTISSEURS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 1, 1, 2, 1, 0 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(82, "GUILDE DES MAGISTRATS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 1, 2, 0, 0, 1 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(83, "GUILDE DES SCIENTIFIQUES", Colour::Purple, { 0, 0, 0, 0, 0 }, { 2, 2, 0, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(84, "GUILDE DES USURIERS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 0, 2, 2, 0, 0 }, 0, 0, 0, 0, 7, 17, 17));
		guilds.push_back(new Card(85, "GUILDE DES TACTICIENS", Colour::Purple, { 0, 0, 0, 0, 0 }, { 1, 0, 2, 0, 1 }, 0, 0, 0, 0, 7, 17, 17));
	}
	void init_Deck_Age1()
	{
		std::random_device rdSeed;
		std::mt19937 gen(rdSeed());
		
		std::vector<Card*> tmpDeck(cards_age1);
		std::shuffle(tmpDeck.begin(), tmpDeck.end(), gen);
		deck_age1.insert(deck_age1.end(), tmpDeck.begin(), tmpDeck.begin() + 20);
	}
	void init_Deck_Age2()
	{
		std::random_device rdSeed;
		std::mt19937 gen(rdSeed());

		std::vector<Card*> tmpDeck(cards_age2);
		std::shuffle(tmpDeck.begin(), tmpDeck.end(), gen);
		deck_age2.insert(deck_age2.end(), tmpDeck.begin(), tmpDeck.begin() + 20);
	}
	void init_Deck_Age3()
	{
		std::random_device rdSeed;
		std::mt19937 gen(rdSeed());

		std::vector<Card*> tmpDeck(cards_age3);
		std::shuffle(tmpDeck.begin(), tmpDeck.end(), gen);
		deck_age3.insert(deck_age3.end(), tmpDeck.begin(), tmpDeck.begin() + 17);

		std::vector<Card*> tmpGuilds(guilds);
		std::shuffle(guilds.begin(), guilds.end(), gen);
		deck_age3.insert(deck_age3.end(), tmpGuilds.begin(), tmpGuilds.begin() + 3);

		std::shuffle(deck_age3.begin(), deck_age3.end(), gen);
	}

	std::vector<Card*>& getWonders() {return wonders;}
	std::vector<Card*>& getCards_Age1() {return cards_age1;}
	std::vector<Card*>& getCards_Age2() {return cards_age2;}
	std::vector<Card*>& getCards_Age3() {return cards_age3;}
	std::vector<Card*>& getGuilds() {return guilds;}
	std::vector<Card*>& getDeck_Age1() {return deck_age1;}
	std::vector<Card*>& getDeck_Age2() {return deck_age2;}
	std::vector<Card*>& getDeck_Age3() {return deck_age3;}
};

#endif // !Deck_h
﻿#ifndef SideBoard_h
#define SideBoard_h

#include "GameLogicHeader.h"

class ProgressToken
{
private:
	unsigned int id;
	std::string name;
	bool takenFlag = false;

public:
	ProgressToken(int i, std::string n) : id(i), name(n), takenFlag(false) {}
	unsigned int& getID() { return id; }
	std::string& getName() { return name; }
	bool& getTakenFlag() { return takenFlag; }
	void setTakenFlag(bool flag) { takenFlag = flag; }
};

class SideBoard
{
private:
	std::array<ProgressToken*, 10> progressTokens;
	std::array<ProgressToken*, 5> progressTokensDeck;
	int conflictPawnPosition = 0;

	bool left_2Coins_taken = false;
	bool left_5Coins_taken = false;

	bool right_2Coins_taken = false;
	bool right_5Coins_taken = false;

public:
	SideBoard()
	{
		progressTokens.at(0) = new ProgressToken(1, "Agriculture");
		progressTokens.at(1) = new ProgressToken(2, "Architecture");
		progressTokens.at(2) = new ProgressToken(3, "Économie");
		progressTokens.at(3) = new ProgressToken(4, "Loi");
		progressTokens.at(4) = new ProgressToken(5, "Maçonnerie");
		progressTokens.at(5) = new ProgressToken(6, "Mathématiques");
		progressTokens.at(6) = new ProgressToken(7, "Philosophie");
		progressTokens.at(7) = new ProgressToken(8, "Stratégie");
		progressTokens.at(8) = new ProgressToken(9, "Théologie");
		progressTokens.at(9) = new ProgressToken(10, "Urbanisme");

		std::random_device rdSeed;
		std::mt19937 gen(rdSeed());
		std::array<ProgressToken*, 10> tmpDeck(progressTokens);
		std::shuffle(tmpDeck.begin(), tmpDeck.end(), gen);
		for (size_t i = 0; i < 5; i += 1) { progressTokensDeck.at(i) = tmpDeck.at(i); }
	}
	~SideBoard()
	{
		for (size_t i = 0; i < 10; i += 1) { delete progressTokens.at(i); }
	}

	std::array<ProgressToken*, 5>& getProgressTokensDeck() { return progressTokensDeck; }
	int& getConflictPawnPosition() { return conflictPawnPosition; }
	bool& getLeft2CoinsTaken() { return left_2Coins_taken; }
	bool& getLeft5CoinsTaken() { return left_5Coins_taken; }
	bool& getRight2CoinsTaken() { return right_2Coins_taken; }
	bool& getRight5CoinsTaken() { return right_5Coins_taken; }
	std::array<bool, 4> getLootingFlags()
	{
		std::array<bool, 4> lootingFlags;
		lootingFlags[0] = left_5Coins_taken;
		lootingFlags[1] = left_2Coins_taken;
		lootingFlags[2] = right_2Coins_taken;
		lootingFlags[3] = right_5Coins_taken;
		return lootingFlags;
	}

	void setConflictPawnPosition(int pos) { conflictPawnPosition = pos; }
	void setLeft2CoinsTaken(bool flag) { left_2Coins_taken = flag; }
	void setLeft5CoinsTaken(bool flag) { left_5Coins_taken = flag; }
	void setRight2CoinsTaken(bool flag) { right_2Coins_taken = flag; }
	void setRight5CoinsTaken(bool flag) { right_5Coins_taken = flag; }
};

#endif // !SideBoard_h
#ifndef GameController_h
#define GameController_h

#include "Deck.h"
#include "SideBoard.h"
#include "Player.h"
#include "QtHeader.h"

class GameController
{
private:
	Deck* deck = nullptr;
	Player* player1 = nullptr;
	Player* player2 = nullptr;
	Player* winner = nullptr;
	SideBoard* sideBoard = nullptr;
	unsigned int numCardTaken = 0;
	unsigned int numWonderBuilt = 0;

	bool verifyChainSymbol(Player* player, Card* card)
	{
		// Get the ID of the chain symbol required by the card
		unsigned int card_CS_id = card->getChainSymbolCost();

		// If the ID is 17, it means the chain symbol is null, so return false
		if (card_CS_id == 17) { return false; }

		// Check if the player has the required chain symbol
		if (player->getChainSymbols()[card_CS_id] > 0) { return true; }

		// If not, return false
		return false;
	}
	bool verifyItems(Player* player, Card* card, std::array<unsigned int, 5>& nums_LackItems)
	{
		// Reset the array of numbers representing lacking items to all zeros
		nums_LackItems = { 0, 0, 0, 0, 0 };

		// Get the array of items needed to construct the card
		std::array<unsigned int, 5> itemsNeeded(card->getItemsCost());

		// Get the array of items possessed by the player
		std::array<unsigned int, 5> playersItems(player->getItems());

		// Initialize a flag to indicate if items are sufficient
		bool itemsSufficientFlag = true;

		// Iterate over each item type: clay, wood, stone, glass, papyrus
		for (size_t i = 0; i < 5; i += 1)
		{
			// If the player has fewer items than needed
			if (playersItems.at(i) < itemsNeeded.at(i))
			{
				// Calculate the number of lacking items and update the corresponding entry in the nums_LackItems array
				nums_LackItems.at(i) = itemsNeeded.at(i) - playersItems.at(i);

				// Set the flag to indicate that items are not sufficient
				itemsSufficientFlag = false;
			}
		}

		// Return the flag indicating if items are sufficient
		return itemsSufficientFlag;
	}
	bool verifyCoins(Player* player, Card* card, const std::array<unsigned int, 5>& nums_LackItems, unsigned int& extraCost)
	{
		// Initialize an array to store items possessed by the competitor player
		std::array<unsigned int, 5> competitorsItems = { 0, 0, 0, 0, 0 };

		// Determine the competitor player and get their items
		if (player != player1) { competitorsItems = player1->getItems(); }
		else { competitorsItems = player2->getItems(); }

		// Initialize extraCost to zero
		extraCost = 0;

		// Iterate over each item type: clay, wood, stone, glass, papyrus
		for (size_t i = 0; i < 5; i += 1)
		{
			// If there are lacking items of this type
			if (nums_LackItems.at(i) > 0)
			{
				// Calculate the extra cost based on the number of lacking items and the number of items possessed by the competitor player
				extraCost += ((2 + competitorsItems.at(i)) * nums_LackItems.at(i));
			}
		}

		// If the player's coins are less than the total cost (including extra cost) required to construct the card, return false; otherwise, return true
		if (player->getCoins() < card->getCoinsCost() + extraCost) { return false; }
		else { return true; }
	}
	bool verifySciSymbols(Player* player)
	{
		// Check if the player has any progress tokens available
		if (player->getNumProgressTokensAvailable() > 0) { return true; }
		else { return false; }
	}
	bool verifyMilitarySupremacy()
	{
		if (sideBoard->getConflictPawnPosition() >= 9)
		{
			winner = player1;
			return true;
		}
		if (sideBoard->getConflictPawnPosition() <= -9)
		{
			winner = player2;
			return true;
		}
		return false;
	}
	bool verifyScientificSupremacy(Player* player)
	{
		// Check if the player is the current player
		if (!player->getCurrentPlayerFlag()) { return false; }

		// Initialize a counter to count the number of different scientific symbols owned by the player
		unsigned int numSciSymDiff = 0;

		// Get the array of scientific symbols possessed by the player
		std::array<unsigned int, 7> plyrsSciSyms = player->getSciSymbols();

		// Iterate over each scientific symbol type
		for (size_t i = 0; i < plyrsSciSyms.size(); i += 1)
		{
			// If the player has at least one of this scientific symbol type, increment the counter
			if (plyrsSciSyms.at(i) > 0) { numSciSymDiff += 1; }
		}

		// Check if the player has at least 6 different scientific symbols
		// If so, declare the player as the winner and return true; otherwise, return false
		if (numSciSymDiff >= 6)
		{
			winner = player;
			return true;
		}
		else { return false; }
	}
	bool verifyCivilianVictory()
	{
		// Get the position of the conflict pawn
		int cpPosition = sideBoard->getConflictPawnPosition();

		// If the conflict pawn is on the right side
		if (cpPosition > 0)
		{
			// Increment player 1's victory points based on the conflict pawn position
			if ((1 <= cpPosition) && (cpPosition <= 2)) { player1->incVicPoint(2); }
			else if ((3 <= cpPosition) && (cpPosition <= 5)) { player1->incVicPoint(5); }
			else if (6 <= cpPosition) { player1->incVicPoint(10); }
		}
		// If the conflict pawn is on the left side
		else if (cpPosition < 0)
		{
			// Increment player 2's victory points based on the conflict pawn position
			if ((-2 <= cpPosition) && (cpPosition <= -1)) { player2->incVicPoint(2); }
			else if ((-5 <= cpPosition) && (cpPosition <= -3)) { player2->incVicPoint(5); }
			else if (cpPosition <= -6) { player2->incVicPoint(10); }
		}

		// Increment player 1 and player 2's victory points based on their coins
		player1->incVicPoint(player1->getCoins() / 3);
		player2->incVicPoint(player2->getCoins() / 3);

		// Determine the winner based on their victory points
		if (player1->getVicPoint() > player2->getVicPoint()) { winner = player1; }
		else if (player1->getVicPoint() < player2->getVicPoint()) { winner = player2; }
		// If the victory points are equal, compare the number of blue cards
		else
		{
			size_t num_p1_blueCards = player1->getCards_Blue().size();
			size_t num_p2_blueCards = player2->getCards_Blue().size();
			if (num_p1_blueCards > num_p2_blueCards) { winner = player1; }
			//////////else if (num_p1_blueCards = num_p2_blueCards)
			else { winner = player2; }
		}

		// Return true indicating the verification of civilian victory
		return true;
	}

	void handleCardObtained_Brown(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the brown card and confirms card acquisition
		player->obtainCard_Brown(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// Get the reference to the array of items produced by the card
		const std::array<unsigned int, 5>& itemsObtained(card->getItemsProd());

		// Get the reference to the player's array of items
		std::array<unsigned int, 5>& playersItems(player->getItems());

		// Add each resource item provided by the card to the player's items
		for (size_t i = 0; i < itemsObtained.size(); i += 1) { playersItems.at(i) += itemsObtained.at(i); }
		
		// Deduct the card's coin cost and extra cost from the player's coins
		player->decCoins(card->getCoinsCost() + extraCost);
	}
	void handleCardObtained_Grey(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the grey card and confirms card acquisition
		player->obtainCard_Grey(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// Get the reference to the array of items produced by the card
		const std::array<unsigned int, 5>& itemsObtained(card->getItemsProd());

		// Get the reference to the player's array of items
		std::array<unsigned int, 5>& playersItems(player->getItems());

		// Add each resource item provided by the card to the player's items
		for (size_t i = 0; i < itemsObtained.size(); i += 1) { playersItems.at(i) += itemsObtained.at(i); }
		
		// Deduct the card's coin cost and extra cost from the player's coins
		player->decCoins(card->getCoinsCost() + extraCost);
	}
	void handleCardObtained_Blue(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the blue card and confirms card acquisition
		player->obtainCard_Blue(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);
		player->incVicPoint(card->getVicPoint());

		// Obtain the chain symbol from the card and add it to the player's collection
		player->obtainChainSymbol(card->getChainSymbolProd());
		
		// If the card isn't constructed for free, deduct the card's coin cost and extra cost from the player's coins
		if (!freeFlag) { player->decCoins(card->getCoinsCost() + extraCost); }
	}
	void handleCardObtained_Green(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the green card and confirms card acquisition
		player->obtainCard_Green(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// Increment player's victory points based on the card's victory point value
		player->incVicPoint(card->getVicPoint());

		// Obtain the scientific symbol from the card and add it to the player's collection
		player->obtainSciSymbol(card->getSciSymbol());

		// If the player has a pair of identical obtained scientific symbols, increase the number of progress tokens available
		if (player->getSciSymbols().at(card->getSciSymbol()) == 2) { player->incNumProgressTokensAvailable(); }
		
		// Obtain the chain symbol from the card and add it to the player's collection
		player->obtainChainSymbol(card->getChainSymbolProd());

		// If the card isn't constructed for free, deduct the card's coin cost and extra cost from the player's coins
		if (!freeFlag) { player->decCoins(card->getCoinsCost() + extraCost); }
	}
	void handleCardObtained_Yellow(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the yellow card and confirms card acquisition
		player->obtainCard_Yellow(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// Increment player's coins based on the card's coin production value
		player->incCoins(card->getCoinsProd());

		// Increment player's victory points based on the card's victory point value
		player->incVicPoint(card->getVicPoint());
		
		// Obtain the chain symbol from the card and add it to the player's collection
		player->obtainChainSymbol(card->getChainSymbolProd());

		// If the card isn't constructed for free, deduct the card's coin cost and extra cost from the player's coins
		if (!freeFlag) { player->decCoins(card->getCoinsCost() + extraCost); }
	}
	void handleCardObtained_Red(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the red card and confirms card acquisition
		player->obtainCard_Red(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// Increase player's military power based on the card's military power value
		player->incMilPower(card->getMilPower());
		
		// Obtain the chain symbol from the card and add it to the player's collection
		player->obtainChainSymbol(card->getChainSymbolProd());

		// If the card isn't constructed for free, deduct the card's coin cost and extra cost from the player's coins
		if (!freeFlag) { player->decCoins(card->getCoinsCost() + extraCost); }
	}
	void handleCardObtained_Purple(Player* player, Card* card, bool freeFlag, unsigned int extraCost, bool confirmFlag)
	{
		// If the card construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Player obtains the purple card and confirms card acquisition
		player->obtainCard_Purple(card, confirmFlag);

		// Set the constructed flag of the card to true
		card->setConstructedFlag(true);

		// If the card isn't constructed for free, deduct the card's coin cost and extra cost from the player's coins
		if (!freeFlag) { player->decCoins(card->getCoinsCost() + extraCost); }
	}
	void handleWonderObtained(Player* player, Card* building, Card* wonder, unsigned int extraCost, bool confirmFlag)
	{
		// If the wonder construction isn't confirmed, return without doing anything
		if (!confirmFlag) { return; }

		// Increment the count of cards taken
		numCardTaken += 1;

		// Increment the count of wonders built
		numWonderBuilt += 1;

		// Set the constructed flag of the card to true
		building->setConstructedFlag(true);

		// Get the array of player's wonders
		std::array<Card*, 4>& playersWonders(player->getWonders());

		// Iterate over each wonder to find the wonder to be constructed
		for (size_t i = 0; i < 4; i += 1)
		{
			// If the wonder is found
			if (playersWonders.at(i) == wonder)
			{
				// Set the constructed flag of the wonder to true
				playersWonders.at(i)->setConstructedFlag(true);

				// Increment player's coins and victory points based on the wonder's production
				player->incCoins(wonder->getCoinsProd());
				player->incVicPoint(wonder->getVicPoint());

				// Deduct the extra cost from the player's coins
				player->decCoins(extraCost);
			}
		}
	}

public:
	GameController() { init_Game(); }
	~GameController()
	{
		delete deck;
		delete player1;
		delete player2;
		delete sideBoard;
	}

	Deck* getDeck() { return deck; }
	Player* getPlayer1() { return player1; }
	Player* getPlayer2() { return player2; }
	Player* getWinner() { return winner; }
	SideBoard* getSideBoard() { return sideBoard; }
	unsigned int& getNumCardTaken() { return numCardTaken; }
	unsigned int& getNumWonderBuilt() { return numWonderBuilt; }

	void init_Game()
	{
		if (deck)
		{
			delete deck;
			deck = nullptr;
		}
		deck = new Deck;

		if (player1)
		{
			delete player1;
			player1 = nullptr;
		}
		player1 = new Player("Player 1");
		player1->setCurrentPlayerFlag(true);

		if (player2)
		{
			delete player2;
			player2 = nullptr;
		}
		player2 = new Player("Player 2");
		player2->setCurrentPlayerFlag(false);

		if (sideBoard)
		{
			delete sideBoard;
			sideBoard = nullptr;
		}
		sideBoard = new SideBoard;

		numCardTaken = 0;
		numWonderBuilt = 0;

		std::random_device rdSeed;
		std::mt19937 gen(rdSeed());

		std::vector<Card*> tmpWonders(deck->getWonders());
		std::shuffle(tmpWonders.begin(), tmpWonders.end(), gen);
		for (size_t i = 0; i < 4; i += 1) { player1->getWonders()[i] = tmpWonders[i]; }
		for (size_t i = 0; i < 4; i += 1) { player2->getWonders()[i] = tmpWonders[i + 4]; }
	}
	char constructBuilding(Player* player, Card* card)
	{
		// If the card has already been discarded, return 'x' indicating an exception
		if (card->getDiscardFlag()) { return 'x'; }

		// If the card has already been constructed, return 'x' indicating an exception
		if (card->getConstructedFlag()) { return 'x'; }

		// If the player isn't the current player, return 'x' indicating an exception
		if (!player->getCurrentPlayerFlag()) { return 'x'; }
		
		// Check if the card can be constructed for free based on chain symbols
		bool freeFlag = verifyChainSymbol(player, card);
		unsigned int extraCost = 0;
		bool confirmFlag = freeFlag;

		if (!freeFlag) // If the card can't be constructed for free, then verify if it can be built by using items or coins
		{
			// Initialize an array who will track the numbers of lacking items
			std::array<unsigned int, 5> nums_LackItems = { 0, 0, 0, 0, 0 };

			// Check if the player has the items needed to construct the card
			// The numbers of missing items will be stored in the nums_LackItems array
			verifyItems(player, card, nums_LackItems);

			// Check if the player has enough coins to construct the card or purchase the missing items
			// Coins used to purchase the items will be stored in extraCost
			// The result will determine if the card can be built and is stored in confirmFlag
			confirmFlag = verifyCoins(player, card, nums_LackItems, extraCost);
		}

		// If the card cannot be constructed even with items and coins, return '0' indicating inability to construct
		if (!confirmFlag) { return '0'; }

		switch (card->getColour())
		{
		case Colour::Brown:
			handleCardObtained_Brown(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Grey:
			handleCardObtained_Grey(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Blue:
			handleCardObtained_Blue(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Green:
			handleCardObtained_Green(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Yellow:
			handleCardObtained_Yellow(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Red:
			handleCardObtained_Red(player, card, freeFlag, extraCost, confirmFlag);
			break;
		case Colour::Purple:
			handleCardObtained_Purple(player, card, freeFlag, extraCost, confirmFlag);
			break;
		default:
			// If the card's colour doesn't match the above types, return 'x' indicating an exception
			return 'x';
			break;
		}

		// If all checks pass and the card can be constructed, return '1' indicating success
		return '1';
	}
	void discardBuilding(Player* player, Card* card)
	{
		if (card->getDiscardFlag()) { return; }
		if (!player->getCurrentPlayerFlag()) { return; }
		numCardTaken += 1;
		card->setDiscardFlag(true);
		unsigned int bonus = player->getCards_Yellow().size();
		player->incCoins(2 + bonus);
	}
	char constructWonder(Player* player, Card* building, Card* wonder)
	{
		// If the wonder card passed in is not a wonder, return 'x' indicating an exception
		if (wonder->getColour() != Colour::Null) { return 'x'; }

		// If the building card has already been already constructed or discarded, return 'x' indicating an exception
		if (building->getConstructedFlag() || building->getDiscardFlag()) { return 'x'; }

		// If the wonder card is already constructed or not constructible, return 'x' indicating an exception
		if (wonder->getConstructedFlag() || !wonder->getConstructibleFlag()) { return 'x'; }

		// If the player isn't the current player, return 'x' indicating an exception
		if (!player->getCurrentPlayerFlag()) { return 'x'; }

		// Check if the number of wonders built is less than 8
		if (numWonderBuilt < 8)
		{
			// Initialize an array to track lacking items
			std::array<unsigned int, 5> nums_LackItems = { 0, 0, 0, 0, 0 };

			// Initialize an extra cost variable
			unsigned int extraCost = 0;

			// Check if the player has the items needed to construct the card
			// The numbers of missing items will be stored in the nums_LackItems array
			verifyItems(player, wonder, nums_LackItems);

			// Check if the player has enough coins to construct the card or purchase the missing items
			// Coins used to purchase the items will be stored in extraCost
			// The result will determine if the card can be built and is stored in confirmFlag
			bool confirmFlag = verifyCoins(player, wonder, nums_LackItems, extraCost);

			// If the card cannot be constructed even with items and coins, return '0' indicating inability to construct
			if (!confirmFlag) { return '0'; }

			// Handle the process of obtaining the wonder
			handleWonderObtained(player, building, wonder, extraCost, confirmFlag);

			// If all checks pass and the wonder can be constructed, return '1' indicating success
			return '1';
		}
		// Return '0' if the number of wonders built exceeds the limit
		return '0';
	}
	void checkLootingToken()
	{
		// Get the conflict pawn position
		int cpPosition = sideBoard->getConflictPawnPosition();
		
		// Get the flags indicating whether coins have been looted from each side
		bool l_2CFlag = sideBoard->getLeft2CoinsTaken();
		bool l_5CFlag = sideBoard->getLeft5CoinsTaken();
		bool r_2CFlag = sideBoard->getRight2CoinsTaken();
		bool r_5CFlag = sideBoard->getRight5CoinsTaken();

		// If the conflict pawn is on the left side and the 2 coins haven't been taken yet
		if ((cpPosition <= -3) && !l_2CFlag)
		{
			// Loot 2 coins from player 1 and store the actual amount looted in the variable loot
			unsigned int loot = player1->decCoins(2);
			// Increment player 2's coins by the looted amount
			player2->incCoins(loot);
			// Set the flag indicating that the 2 coins on the left side have been taken
			sideBoard->setLeft2CoinsTaken(true);
		}

		// If the conflict pawn is on the left side and the 5 coins haven't been taken yet
		if ((cpPosition <= -6) && !l_5CFlag)
		{
			// Loot 5 coins from player 1 and store the actual amount looted in the variable loot
			unsigned int loot = player1->decCoins(5);
			// Increment player 2's coins by the looted amount
			player2->incCoins(loot);
			// Set the flag indicating that the 5 coins on the left side have been taken
			sideBoard->setLeft5CoinsTaken(true);
		}

		// If the conflict pawn is on the right side and the 2 coins haven't been taken yet
		if ((cpPosition >= 3) && !r_2CFlag)
		{
			// Loot 2 coins from player 2 and store the actual amount looted in the variable loot
			unsigned int loot = player2->decCoins(2);
			// Increment player 1's coins by the looted amount
			player1->incCoins(loot);
			// Set the flag indicating that the 2 coins on the right side have been taken
			sideBoard->setRight2CoinsTaken(true);
		}

		// If the conflict pawn is on the right side and the 5 coins haven't been taken yet
		if ((cpPosition >= 6) && !r_5CFlag)
		{
			// Loot 5 coins from player 2 and store the actual amount looted in the variable loot
			unsigned int loot = player2->decCoins(5);
			// Increment player 1's coins by the looted amount
			player1->incCoins(loot);
			// Set the flag indicating that the 5 coins on the right side have been taken
			sideBoard->setRight5CoinsTaken(true);
		}
	}
	int updateSideBoard()
	{
		// Calculate the difference in military power between player 1 and player 2
		int MilPowerDiff = player1->getMilPower() - player2->getMilPower();

		// Set the conflict pawn position on the side board based on the military power difference
		sideBoard->setConflictPawnPosition(MilPowerDiff);

		// Check if any coins need to be looted based on the conflict pawn position
		checkLootingToken();

		// Return the military power difference
		return MilPowerDiff;
	}
	char takeProgressToken(Player* player, ProgressToken* progressToken)
	{
		// If the progress token has already been taken, return '0' indicating inability to take
		if (progressToken->getTakenFlag()) { return '0'; }

		// Verify if the player has pairs of scientific symbols to take the progress token
		bool flag = verifySciSymbols(player);

		// If the player doesn't have pairs of scientific symbols, return '0' indicating inability to take
		if (!flag) { return '0'; }

		// Obtain the progress token and mark it as taken by the player
		player->obtainProgressTokens(progressToken, flag);

		// Return '1' indicating success in taking the progress token
		return '1';
	}
	bool checkVictoryConditions()
	{
		// Check military supremacy condition
		bool condition1 = verifyMilitarySupremacy();

		// Check scientific supremacy condition for player 1
		bool condition2 = verifyScientificSupremacy(player1);

		// Check scientific supremacy condition for player 2
		bool condition3 = verifyScientificSupremacy(player2);

		// If all cards have been taken, verify civilian victory condition
		if (numCardTaken == 60) { return verifyCivilianVictory(); }

		// Otherwise, return true if any of the victory conditions are met
		return condition1 || condition2 || condition3;
	}
};

#endif // !GameController_h
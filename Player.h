#ifndef Player_h
#define Player_h

#include "GameLogicHeader.h"
#include "Card.h"
#include "SideBoard.h"

class Player
{
private:
	std::string name;
	/////unsigned int coins = 7;
	unsigned int coins = 99;
	std::array<unsigned int, 5> items{};
	/////std::array<unsigned int, 5> items{99, 99, 99, 99, 99};
	unsigned int vic_point = 0;
	unsigned int mil_power = 0;
	std::array<unsigned int, 7> sciSymbols{};
	std::array<unsigned int, 17> chainSymbols{};
	std::vector<ProgressToken*> progressTokens;
	unsigned int numProgressTokensAvailable = 0;
	std::vector<Card*> cards_brown;
	std::vector<Card*> cards_grey;
	std::vector<Card*> cards_blue;
	std::vector<Card*> cards_green;
	std::vector<Card*> cards_yellow;
	std::vector<Card*> cards_red;
	std::vector<Card*> cards_purple;
	std::array<Card*, 4> wonders{};
	bool currentPlayerFlag = false;

public:
	/////Player(std::string n) : name(n), coins(7), vic_point(0), mil_power(0) {}
	Player(std::string n) : name(n), coins(99), vic_point(0), mil_power(0) {}
	std::string& getName() {return name;}
	unsigned int& getCoins() {return coins;}
	unsigned int& getVicPoint() {return vic_point;}
	unsigned int& getMilPower() {return mil_power;}
	std::array<unsigned int, 5>& getItems() {return items;}
	std::array<unsigned int, 7>& getSciSymbols() {return sciSymbols;}
	std::array<unsigned int, 17>& getChainSymbols() {return chainSymbols;}
	std::vector<ProgressToken*>& getProgressTokens() { return progressTokens; }
	unsigned int& getNumProgressTokensAvailable() { return numProgressTokensAvailable; }
	std::vector<Card*>& getCards_Brown() {return cards_brown;}
	std::vector<Card*>& getCards_Grey() {return cards_grey;}
	std::vector<Card*>& getCards_Blue() {return cards_blue;}
	std::vector<Card*>& getCards_Green() {return cards_green;}
	std::vector<Card*>& getCards_Yellow() {return cards_yellow;}
	std::vector<Card*>& getCards_Red() {return cards_red;}
	std::vector<Card*>& getCards_Purple() {return cards_purple;}
	std::vector<Card*> getCards()
	{
		std::vector<Card*> tmpCards;
		tmpCards.insert(tmpCards.end(), cards_brown.begin(), cards_brown.end());
		tmpCards.insert(tmpCards.end(), cards_grey.begin(), cards_grey.end());
		tmpCards.insert(tmpCards.end(), cards_blue.begin(), cards_blue.end());
		tmpCards.insert(tmpCards.end(), cards_green.begin(), cards_green.end());
		tmpCards.insert(tmpCards.end(), cards_yellow.begin(), cards_yellow.end());
		tmpCards.insert(tmpCards.end(), cards_red.begin(), cards_red.end());
		tmpCards.insert(tmpCards.end(), cards_purple.begin(), cards_purple.end());
		return tmpCards;
	}
	std::array<Card*, 4>& getWonders() { return wonders; }
	bool& getCurrentPlayerFlag() { return currentPlayerFlag; }
	void setCurrentPlayerFlag(bool flag) { currentPlayerFlag = flag; }

	void incCoins(unsigned int ic) { coins += ic; }
	void incItem(unsigned int itID, unsigned int amount) { items.at(itID) += amount; }
	void incVicPoint(unsigned int iv) { vic_point += iv; }
	void incMilPower(unsigned int im) { mil_power += im; }
	void incNumProgressTokensAvailable() { numProgressTokensAvailable += 1; }

	unsigned int decCoins(unsigned int dc)
	{
		if (coins >= dc)
		{
			coins -= dc;
			return dc;
		}
		else
		{
			int tmpVal = coins;
			coins = 0;
			return tmpVal;
		}
	}
	void decItem(unsigned int itID, unsigned int amount)
	{
		if (items.at(itID) >= amount) { items.at(itID) -= amount; }
		else { items.at(itID) = 0; }
	}
	void decVicPoint(unsigned int dv)
	{
		if (vic_point >= dv) { vic_point -= dv; }
		else { vic_point = 0; }
	}
	void decMilPower(unsigned int dm)
	{
		if (mil_power >= dm) { mil_power -= dm; }
		else { mil_power = 0; }
	}
	void decNumProgressTokensAvailable()
	{
		if (numProgressTokensAvailable >= 1) { numProgressTokensAvailable -= 1; }
	}

	void obtainProgressTokens(ProgressToken* pt, bool obtainableFlag)
	{
		if (!obtainableFlag) { return; }
		progressTokens.push_back(pt);
		pt->setTakenFlag(true);
		numProgressTokensAvailable -= 1;
	}
	void obtainSciSymbol(unsigned int ssID)
	{
		if (ssID == 7) { return; }
		sciSymbols.at(ssID) += 1;
	}
	void obtainChainSymbol(unsigned int csID)
	{
		if (csID == 17) { return; }
		chainSymbols.at(csID) += 1;
	}
	void obtainCard_Brown(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_brown.push_back(card);
	}
	void obtainCard_Grey(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_grey.push_back(card);
	}
	void obtainCard_Blue(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_blue.push_back(card);
	}
	void obtainCard_Green(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_green.push_back(card);
	}
	void obtainCard_Yellow(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_yellow.push_back(card);
	}
	void obtainCard_Red(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_red.push_back(card);
	}
	void obtainCard_Purple(Card* card, bool confirmFlag)
	{
		if (!confirmFlag) { return; }
		cards_purple.push_back(card);
	}
	void obtainWonder(Card* card, Card* wdr, bool obtainableFlag, unsigned int& extraCost)
	{
		if (!obtainableFlag) { return; }
		Colour clr = wdr->getColour();
		if (clr != Colour::Null) { return; }
		card->setConstructedFlag(true);
		for (size_t i = 0; i < 4; i += 1)
		{
			if (wonders.at(i) == wdr)
			{
				wonders.at(i)->setConstructedFlag(true);
				incCoins(wdr->getCoinsProd());
				incVicPoint(wdr->getVicPoint());
				decCoins(card->getCoinsCost() + extraCost);
			}
		}
	}
	
	void distributeWonders(std::array<Card*, 4> wdrs) { wonders = wdrs; }
	void constructWonder(unsigned int wonderID, bool constructibleFlag)
	{
		if (!constructibleFlag) { return; }
		for (auto it = wonders.begin(); it != wonders.end(); ++it)
		{
			if ((*it)->getID() == wonderID) { (*it)->setConstructedFlag(true); }
		}
	}

};

#endif // !Player_h